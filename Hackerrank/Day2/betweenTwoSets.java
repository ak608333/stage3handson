import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result
{

    /*
     * Complete the 'getTotalX' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY a
     *  2. INTEGER_ARRAY b
     */
    public static boolean check_b(int ni,int value,List<Integer> ai)
    {
        for(int i=0;i<ni;i++)
        {
            if(ai.get(i)%value!=0)
            return false;
        }
        return true;
    }
   public static  boolean check_a(int ni,int value,List<Integer> ai)
    {
        for(int i=0;i<ni;i++)
        {
            if(value%a.get(i)!=0)
            return false;
        }
        return true;
    }   
    public static int getTotalX(List<Integer> ai, List<Integer> bi)
    
    
    {
    // Write your code here
    int max_a=-1,min_b=-1;
    int n=ai.size();
    
    
    int m=bi.size();
    for(int i=0;i<n;i++)
    {
       
        if(max_a<ai.get(i))
        max_a=ai.get(i);
    }
    for(int i=0;i<m;i++)
    {
        if(min_b<bi.get(i))
        min_b=bi.get(i);
    
    }
    int coun=0;
    for(int i=max_a;i<=min_b;i++)
    {
        if(check_a(n,i,ai) && check_b(m,i,bi))
        
        coun++;
    }
    
    return coun;

    }
    

}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader buffered = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] firstMultipleInput = buffered.readLine().replaceAll("\\s+$", "").split(" ");

        int n = Integer.parseInt(firstMultipleInput[0]);

        int m = Integer.parseInt(firstMultipleInput[1]);

        List<Integer> arr = Stream.of(buffered.readLine().replaceAll("\\s+$", "").split(" "))
            .map(Integer::parseInt)
            .collect(toList());

        List<Integer> brr = Stream.of(buffered.readLine().replaceAll("\\s+$", "").split(" "))
            .map(Integer::parseInt)
            .collect(toList());

        int total = Result.getTotalX(arr, brr);

        bufferedWriter.write(String.valueOf(total));
        bufferedWriter.newLine();

        buffered.close();
        bufferedWriter.close();
    }
}

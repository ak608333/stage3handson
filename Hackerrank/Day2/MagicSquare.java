import java.io.*;
import java.util.*;

public class Solution
{

    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int[][] m1 = new int[3][3];
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++) {
                m1[x][y] = sc.nextInt();
            }
        }
        int[][][] p = {{{8, 1, 6}, {3, 5, 7}, {4, 9, 2}},
                    {{6, 1, 8}, {7, 5, 3}, {2, 9, 4}},
                    {{8, 3, 4}, {1, 5, 9}, {6, 7, 2}}, 
                    {{6, 7, 2}, {1, 5, 9}, {8, 3, 4}}, 
                    {{4, 9, 2}, {3, 5, 7}, {8, 1, 6}}, 
                    {{2, 9, 4}, {7, 5, 3}, {6, 1, 8}}, 
                    {{2, 7, 6}, {9, 5, 1}, {4, 3, 8}}, 
                    {{4, 3, 8}, {9, 5, 1}, {2, 7, 6}}};
        int m = 100;
        for (int i = 0; i < 8; i++) 
        
        {
            int diff1 = 0;
            for (int x = 0; x < 3; x++) 
            
            {
                for (int y = 0; y < 3; y++)
                
                {
                    diff1 += Math.abs(p[i][x][y] - m1[x][y]);
                }
            }
            if (diff1 < min)
                min = diff1;
        }
        System.out.println(min);
    }
}
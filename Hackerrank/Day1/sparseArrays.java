import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the matchingStrings function below.
    static int[] matchingStrings(String[] strings, String[] querie) 
    {

        int[] a=new int[querie.length];
        
        Map< String,Integer> hm =  new HashMap< String,Integer>(); 
        for(int i=0;i<strings.length;i++){
            if(hm.containsKey(strings[i]))
                hm.put(strings[i],hm.get(strings[i])+1);
            else
                hm.put(strings[i],new Integer(1));
        }
        for(int i=0;i<querie.length;i++){
            if(hm.containsKey(querie[i]))
                    a[i]= hm.get(querie[i]);
            else
                    a[i]=0;
        }
        return a;
    }

    private static final Scan scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException 
    {
        BufferedWriter buffere = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int stringsCount = scan.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String[] strings = new String[stringsCount];

        for (int i = 0; i < stringsCount; i++)
        {
            String stringsItem = scan.nextLine();
            strings[i] = stringsItem;
        }

        int queriesCount = scan.nextInt();
        scan.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String[] queries = new String[queriesCount];

        for (int i = 0; i < queriesCount; i++)
        {
            String queriesItem = scanner.nextLine();
            queries[i] = queriesItem;
        }

        int[] res = matchingStrings(strings, queries);

        for (int i = 0; i < res.length; i++) {
            buffer.write(String.valueOf(res[i]));

            if (i != res.length - 1) {
                buffer.write("\n");
            }
        }

        buffer.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}

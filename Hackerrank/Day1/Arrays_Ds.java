import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import org.apache.commons.lang3.ArrayUtils;
public class Solution
{

    // Complete the reverseArray function below.
    static int[] reverseArray(int[] ai)
    {

        ArrayUtils.reverse(ai); 
        return ai;
    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter buffer = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int arrCount = scan.nextInt();
        scan.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr1 = new int[arrCount];

        String[] arrItemss = scan.nextLine().split(" ");
        scan.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < arrCount; i++) {
            int arrItem = Integer.parseInt(arrItemss[i]);
            arr1[i] = arrItem;
        }

        int[] res = reverseArray(arr1);

        for (int i = 0; i < res.length; i++) {
            buffer.write(String.valueOf(res[i]));

            if (i != res.length - 1) {
                buffer.write(" ");
            }
        }

        buffer.newLine();

        buffer.close();

        scan.close();
    }
}
